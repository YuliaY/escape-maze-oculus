﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterCollisionsProblem : MonoBehaviour

{

    public GameObject OVRCameraRig;
    private CollisionProblem CollisionProblem;

    private void Start()
    {
        CollisionProblem = OVRCameraRig.GetComponent<CollisionProblem>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (TriggerVRObject(other))
            CollisionProblem.enabled = false;

    }

    private void OnTriggerExit(Collider other)
    {
        if (TriggerVRObject(other))
            CollisionProblem.enabled = true;

    }

    private bool TriggerVRObject(Collider other)
    {
        if (other.gameObject == OVRCameraRig)
            return true;
        else
            return false;

    }

}