﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageTrap : MonoBehaviour
{
    public GameObject cage;
   

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag== "Player")
        {
          Instantiate(cage, other.transform.position, other.transform.rotation);
        }
    }

   }
