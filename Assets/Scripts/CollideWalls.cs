﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollideWalls : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Maze")
        {
           GameManager.Singleton.ReturnToSpawnPoint();
            //Debug.Log("Bah");
            // SceneManager.LoadScene(0);
        }
        if (other.gameObject.tag == "DeadZone")
        {
            SceneManager.LoadScene(0);
        }
    }   
}
