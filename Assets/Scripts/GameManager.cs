﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Singleton;

    private Animation ani;
    public GameObject timerHost;
    public GameObject maze;
    public GameObject startButton;
    public bool gameStarted = false;
    public Transform[] spawnPoints;
   // public GameObject spawnPlayer;
  


    private void Awake()
    {
        if (Singleton == null)
        {
            Singleton = this;
        }
        else if (Singleton != this)
        {
            Destroy(gameObject);
        }

        startButton.SetActive(true);

        ReturnToSpawnPoint();

    }
    public void ReturnToSpawnPoint()
    {
        Vector3 spawnpos = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
        this.gameObject.transform.position = spawnpos;

    }

    public void StartTheGame()
    {
        gameStarted = true;
        startButton.SetActive(false);
        maze.GetComponent<Animator>().enabled = true;
        timerHost.GetComponent<Timer>().stop = false;
    }

}
