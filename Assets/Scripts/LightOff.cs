﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightOff : MonoBehaviour
{
    public Light lightSource;
    public GameObject lightTrap;
  //  bool lightTurnOff = false;
    float darkTime = 5f;
    

    private void OnTriggerEnter(Collider other)      
    {
        if (other.gameObject.tag == "Player" && lightSource.enabled)
        {
            lightSource.enabled = false;
            // lightTurnOff = true;
        }
    }

    private void Update()
    {
        if (!lightSource.enabled)
        {
            darkTime -= Time.deltaTime;
            if (darkTime < 0)
            {
                lightSource.enabled = true;
                darkTime = 5f;
            }
        }
    }
    
    

}
