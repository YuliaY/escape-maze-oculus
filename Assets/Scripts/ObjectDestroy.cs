﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroy : MonoBehaviour
{
   
    private void OnCollisionEnter(Collision destroyObj)
    {
        if (destroyObj.gameObject.name == "Cage Variant")
        {
            Destroy(destroyObj.gameObject);

        }
    }
}
