﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSpeedUp : MonoBehaviour
{
    [SerializeField] Timer speedUpTimer;
    public GameObject timeTrap;
    public float speedUpTime = 5;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            speedUpTimer.timefactor = 3;
        }
    }
    private void Update()
    { 
    speedUpTime -= Time.deltaTime;
        speedUpTime = Mathf.Clamp(speedUpTime, 0f, 5f);

            if (speedUpTime <= 0)
            {
            speedUpTimer.timefactor = 1;
            speedUpTime = 5f;
            }
        }
}
