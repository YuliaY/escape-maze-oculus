﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] OVRScreenFade fade;
    


	public Text timerText;
    public float totalTime=300;
    public float timefactor = 1f;
	public GameObject RestartButton;
    

    public bool stop;

    public void Awake()
    {
		RestartButton.SetActive(false);
    }
    /*
    private IEnumerator updateCoroutine()

    {
		while (!stop)
		{
           totalTime -= timefactor * Time.deltaTime;
            string minutes = (totalTime / 60).ToString("00");
			string seconds = (totalTime % 60).ToString("00");
			timerText.text = minutes + ":" + seconds;
			//timerText.text = string.Format("{0:0}:{1:00}", minutes, seconds);
			yield return new WaitForSeconds(0.2f);
		}
		
		if (totalTime <= 0f)
		{

			timerText.text = "00:00";

			StartCoroutine(fade.Fade(1, 0));

			stop = true;
            RestartButton.SetActive(true);
        }

	}*/
         
	// Update is called once per frame
	void FixedUpdate()
     
    {
        if (!stop)
        {
			//totalTime -= Time.time;
            totalTime -= timefactor * Time.deltaTime;
            string minutes = Mathf.Floor(totalTime / 60).ToString("00");
            string seconds = Mathf.Floor(totalTime % 60).ToString("00");

            timerText.text = minutes + ":" + seconds;
        }

        if (totalTime <= 0f)
        {

            timerText.text = "00:00";

            StartCoroutine(fade.Fade(1, 0));

            stop = true;
			RestartButton.SetActive(true);
            
              
        }

    } 
    /*
        //public float timeLeft = 300.0f;
        public bool stop = true;

        private float minutes;
        private float seconds;

        //public Text text;

        public void startTimer(float from)
        {
          //  stop = false;
            totalTime = from;
            Update();
            StartCoroutine(updateCoroutine());
        }

        void Update()
        {
          //  if (stop) return;
            totalTime -= Time.deltaTime;

            minutes = Mathf.Floor(totalTime / 60);
            seconds = totalTime % 60;
            if (seconds > 59) seconds = 59;
            if (minutes < 0)
            {
                stop = true;
                minutes = 0;
                seconds = 0;
            }
            //        fraction = (timeLeft * 100) % 100;
        }

        private IEnumerator updateCoroutine()
        {
            while (!stop)
            {
                timerText.text = minutes + ":" + seconds;
                //timerText.text = string.Format("{0:0}:{1:00}", minutes, seconds);
                yield return new WaitForSeconds(0.2f);
            }
        }*/



    /*   void Update()
    {
        totalTime -= Time.deltaTime;
        timerText.text = "Time Left:" + Mathf.Round(totalTime);
        if (totalTime < 0)
        {


            // StartCoroutine(fade.Fade(1, 0));
            //Application.LoadLevel("gameOver");
        }
    }*/

   


}
